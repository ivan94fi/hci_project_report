MEDIA:

1- L’iscrizione all’app "UniFiAdvisor" è facile e veloce                                                                                                           6.888889
2- Non appena si accede è intuitivo come ricercare un corso specifico                                                                                              6.555556
3- La pagina del corso offre un’ organizzazione delle informazioni peggiore rispetto a quella del sito ufficiale                                                   1.111111
1- Non è chiaro come poter creare un nuovo piano di studi                                                                                                          1.222222
2- L’aggiunta e la rimozione di esami da un piano di studi è semplice ed immediata                                                                                 5.888889
3- Nel caso di più di un piano di studi la gestione degli esami diventa confusionaria e/o dispersiva                                                               1.888889
1- Non è chiaro che il commento è facoltativo                                                                                                                      1.111111
2- Nella pagina di un corso non è abbastanza evidente come passare alle pagine dei docenti che tengono quel corso e viceversa                                      1.777778
3- In caso di recensione senza commento è chiaro come modificare/eliminare la valutazione                                                                          6.111111
4- Nel caso in cui non si inserisca un commento scritto, è comunque evidente che il tuo voto numerico viene conteggiato nella votazione finale                     6.222222
1- L’applicazione rende facile raccogliere informazioni sui docenti e sui corsi.                                                                                   6.666667
2- La possibilità di creare Piani Di Studi è una funzionalità che dovrebbe essere sviluppata ulteriormente, aggiungendo il controllo automatico della validità.    4.888889
3- Nel caso fossi uno studente iscritto all’Università degli Studi di Firenze userei questa applicazione                                                           6.333333


VARIANZA:

1- L’iscrizione all’app "UniFiAdvisor" è facile e veloce                                                                                                           0.111111
2- Non appena si accede è intuitivo come ricercare un corso specifico                                                                                              0.527778
3- La pagina del corso offre un’ organizzazione delle informazioni peggiore rispetto a quella del sito ufficiale                                                   0.111111
1- Non è chiaro come poter creare un nuovo piano di studi                                                                                                          0.194444
2- L’aggiunta e la rimozione di esami da un piano di studi è semplice ed immediata                                                                                 3.861111
3- Nel caso di più di un piano di studi la gestione degli esami diventa confusionaria e/o dispersiva                                                               1.111111
1- Non è chiaro che il commento è facoltativo                                                                                                                      0.111111
2- Nella pagina di un corso non è abbastanza evidente come passare alle pagine dei docenti che tengono quel corso e viceversa                                      0.944444
3- In caso di recensione senza commento è chiaro come modificare/eliminare la valutazione                                                                          2.611111
4- Nel caso in cui non si inserisca un commento scritto, è comunque evidente che il tuo voto numerico viene conteggiato nella votazione finale                     0.944444
1- L’applicazione rende facile raccogliere informazioni sui docenti e sui corsi.                                                                                   0.250000
2- La possibilità di creare Piani Di Studi è una funzionalità che dovrebbe essere sviluppata ulteriormente, aggiungendo il controllo automatico della validità.    1.361111
3- Nel caso fossi uno studente iscritto all’Università degli Studi di Firenze userei questa applicazione                                                           0.250000

